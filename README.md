# Database Movie Theater Project



## Course Project Description


### Goals

1. Be able to design and implement a database for a small application.

2. Be able to create a GUI application to programmatically interact with a relational database management system.


### Deliverables

1. Project Proposal: A brief description of the application you are building for the project. The project proposal must include a description of the database you intend to design, implement, and interact via the GUI application.

2. Intermediate Project Status Report: Each team will submit an up-to-date project timeline document showing tasks/components completed, delayed, planned to be on time, and in risk of being delayed.

3. Project Report: This is the description of the system you designed and implemented. This will contain the languages, framework, RDBMS, etc. used to implement the system, functionalities provided by the GUI application, screenshots of different functionalities in action, etc.

4. ER Diagrams: Submit three images (GIF, PNG, JPG) of your ER Diagrams corresponding to the conceptual, logical, and physical designs. You may (optionally) also include the .vpp file if you used Visual Paradigm to generate the ER-Diagram. Name format for the ERD files:

#### ERD_Conceptual_<team_name>.[jpg | png | gif]
#### ERD_Logical_<team_name>.[jpg | png | gif]
#### ERD_Physical_<team_name>.[jpg | png | gif]

5. Database Creation Script: Submit a single SQL file containing the SQL commands you used to create your database. Include any other SQL scripts you used to prepopulate the database if necessary. 
Name format for the SQL script: Database_Script_<team_name>.sql.

6. Source Code: Compress the entire folder structure of your source code for the application in a zip file and upload. 
Name format for the source code: Source_Code_<team_name>.zip.

7. Presentation Slides: Submit the Microsoft PowerPoint (or equivalent) slides that you will be using during your project presentation. 
Name format for the slide-deck: Presentation_Slides_<team_name>.pptx.

8. Team Report: A document stating how the team completed the work for the assignment (team dynamics). This should include a high -level description of what work each team member did (who did what). The final version of the timeline document may be used for this. Team report should also include an estimated amount of time each team member spent working on their tasks. Explicitly state the time each team member worked on the project for each task they have worked on. Include the team meeting times. 
Name format for the team report file: Team_Report_<team_name>.pdf.

9. Project Individual Report: Each student will submit an individual report in the separate assignment on canvas. The individual report will include more specific details on what you have worked on. This differs from the team report, which will include only higher-level task information, where lower-level details can be discussed in this report. In addition to a more detailed description, any discrepancies in the team report can be included as well as any issues that you feel are important but did not feel comfortable including in the team report.
Name format for the individual report file: Individual_Report_<your_first_name>_<your_last_name>_<team_name>.pdf.


### Requirements

#### Database

Must use one of the following Relational Database Management Systems
- PostgreSQL
- SQL Server (Express)
- MySQL
- Oracle (Express)
- Other (consult with me before starting the project)

Databse must consist of 12 tables with the following:
Primary keys
- Foreign keys
- Unique keys
- Other constraints (e.g., not null, check, multi-attribute unique, etc.)
- Views
- Triggers
- Indices
- Procedures and/or functions
- Cursors
- At least 2 medium-level queries and at least 1 hard-level query.


#### ER Diagrams

Be sure to include the following diagrams
- The Conceptual Design
- The Logical Design
- The Physical Design


#### Application

Create an application with a graphical user interface to interact with the database.

1. The application should support password-based user authentication, and support two types of users:
- Administrator: Will have authorization to perform tasks that includes any schema and data manipulations and have access to all data in the database.
- End-user: Will have limited authorization to perform tasks that includes query to view only the data pertaining to the user and modify user- specific (e.g., personal information, etc.) data only.

2. The application should operate on the database in the following ways
- Insert data
- Select data
- Update data
- Delete data (virtually if not physically)

3. The application should validate all input values before using those in any SQL statement.

4. The application should be error-free. The user interface does not need to be fancy and just needs to be functional.

5. There are no size requirements (such as lines of code) for the application.

6. The application can be a standard desktop application or a browser-based web application.

7. Programming languages permitted
- Java (use JDBC)
- C# (use Npgsql for PostgreSQL)
- C++ (use libpq++ for PostgreSQL)
- Ruby
- Python
- PHP and other web development languages
- Other (consult with me for other languages)


#### Project Presentation

Each team will prepare a 15-minute presentation to present during the final exam period. The project presentation should consist of slides (PowerPoint or Google)
and a demonstration of the application. The presentation slides should contain the following:
 
- Team name
- Team members
- RDBMS choice
- Programming language choice
- Description of application
- ER-Diagram of the database

The slide presentation will be followed by your live demonstration of the application. Slide presentation part should be about 5-minute long, demo about 7-minute long, leaving 2 minutes for Q/A.
